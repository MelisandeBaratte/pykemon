import pygame
import pyscroll
import pytmx


class Game:
    def __init__(self):
        # création de la fenêtre de jeu
        self.screen = pygame.display.set_mode((800, 600))
        pygame.display.set_caption("Pykemon adventure")

        # chargement de la carte (tmx)
        # récupération de la source du fichier
        tmx_data = pytmx.util_pygame.load_pygame('carte.tmx')
        # récupération des données contenu dans le fichier tmx
        map_data = pyscroll.data.TiledMapData(tmx_data)
        # chargement des différents calques à l'intérieur
        map_layer = pyscroll.orthographic.BufferedRenderer(map_data, self.screen.get_size())
        # zoom
        # map_layer.zoom = 2

        # dessiner le groupe de calques
        # default_layer : donne la position du calque par défaut, utile pour placer le joueur à un certain niveau de claque
        self.group = pyscroll.PyscrollGroup(map_layer=map_layer, default_layer=1)

    def run(self):
        # création de la boucle de jeu : maintient en activité la fenêtre de jeu et actualise les composants
        # fenêtre active par défaut
        running = True

        # Tant que la fenêtre est active
        while running:
            # récupération de self.group pour dessiner les calques
            self.group.draw(self.screen)
            # actualisation de l'ensemble
            pygame.display.flip()

            # Boucle sur les évenements actifs
            for event in pygame.event.get():
                # Et si l'évenement courant de type pygame.QUIT est activé
                if event.type == pygame.QUIT:
                    # Alors la fenêtre se ferme
                    running = False

        pygame.quit()
