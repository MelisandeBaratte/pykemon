import pygame

from game import Game

# Génére la base du module
if __name__ == '__main__':
    # initialisation des composants pygame
    pygame.init()
    # instanciation de la classe Game
    # puis exécution
    game = Game()
    game.run()
